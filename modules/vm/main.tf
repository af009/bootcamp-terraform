# Create Virtual Machine
resource "azurerm_linux_virtual_machine" "vm1" {
  name                            = "vm1"
  resource_group_name             = var.resource-group-name
  location                        = var.resource-group-location
  size                            = "Standard_B1ms"
  admin_username                  = var.admin_username
  admin_password                  = var.admin_password
  disable_password_authentication = false

  network_interface_ids = [
    var.nic-id
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts"
    version   = "latest"
  }

  # Remote provisioner added to automate some commands
  provisioner "remote-exec" {

    connection {
      type     = "ssh"
      user     = var.admin_username
      password = var.admin_password
      host     = var.host-ip

    }

    inline = [
      "sudo apt update && sudo apt install npm -y && sudo npm i -g n && sudo n 12",

      "git clone https://github.com/af009/bootcamp-app.git && cd bootcamp-app && sudo npm install"

    ]
  }
}

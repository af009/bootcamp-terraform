# Configure the Azure provider
provider "azurerm" {
  features {}
}

# Create Resource Group
resource "azurerm_resource_group" "rg-basic" {
  name     = "bootcamp-terraform"
  location = "westeurope"
}

# Create Virtual Network
resource "azurerm_virtual_network" "vnet-basic" {
  name                = "vnet"
  address_space       = ["10.16.0.0/25"]
  location            = azurerm_resource_group.rg-basic.location
  resource_group_name = azurerm_resource_group.rg-basic.name
}

# Create Public Subnet (APP)
resource "azurerm_subnet" "subnet-public" {
  name                 = "subnet-public"
  resource_group_name  = azurerm_resource_group.rg-basic.name
  virtual_network_name = azurerm_virtual_network.vnet-basic.name
  address_prefixes     = ["10.16.0.0/28"]
}

# Create Private Subnet (DataBase)
resource "azurerm_subnet" "subnet-private" {
  name                 = "subnet-db"
  resource_group_name  = azurerm_resource_group.rg-basic.name
  virtual_network_name = azurerm_virtual_network.vnet-basic.name
  address_prefixes     = ["10.16.0.16/28"]
  service_endpoints    = ["Microsoft.Sql"]
}

# Create Public IP
resource "azurerm_public_ip" "public-ip" {
  allocation_method   = "Static"
  location            = azurerm_resource_group.rg-basic.location
  name                = "public-ip"
  resource_group_name = azurerm_resource_group.rg-basic.name

}

#Create Network Security Group
resource "azurerm_network_security_group" "nsg" {
  location            = azurerm_resource_group.rg-basic.location
  name                = "nsg"
  resource_group_name = azurerm_resource_group.rg-basic.name

  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "SSH"
    priority                   = 1001
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"

  }
  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "Port-8080"
    priority                   = 100
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    access                     = "Allow"
    direction                  = "Outbound"
    name                       = "sql"
    priority                   = 110
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5432"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

}

# Create Network Interface
resource "azurerm_network_interface" "NIC" {
  name                = "NIC"
  location            = azurerm_resource_group.rg-basic.location
  resource_group_name = azurerm_resource_group.rg-basic.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet-public.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.public-ip.id
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "association" {
  network_interface_id      = azurerm_network_interface.NIC.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}

# Import Virtual Machine

module "vm" {
  source = "./modules/vm"

  resource-group-name = azurerm_resource_group.rg-basic.name
  resource-group-location = azurerm_resource_group.rg-basic.location
  admin_username = var.admin_username
  admin_password = var.admin_password
  nic-id = azurerm_network_interface.NIC.id
  host-ip = azurerm_public_ip.public-ip.ip_address

}

# Import Postgresql Module
module "postgresql" {
  source  = "Azure/postgresql/azurerm"
  version = "2.1.0"

  resource_group_name = azurerm_resource_group.rg-basic.name
  location            = azurerm_resource_group.rg-basic.location
  server_name                  = "data-base-testeo"
  sku_name                     = "GP_Gen5_2"
  storage_mb                   = 5120
  backup_retention_days        = null
  geo_redundant_backup_enabled = false
  administrator_login          = var.PGUSERNAME
  administrator_password       = var.PGPASSWORD
  server_version               = "11"
  ssl_enforcement_enabled      = false
  db_names                     = [var.PGDATABASE]
  db_charset                   = "UTF8"
  db_collation                 = "English_United States.1252"

  firewall_rule_prefix = "firewall-"
  firewall_rules = [
    { name = "app", start_ip = azurerm_public_ip.public-ip.ip_address, end_ip = azurerm_public_ip.public-ip.ip_address },
  ]

  vnet_rule_name_prefix = "postgresql-vnet-rule-"
  vnet_rules = [
    { name = azurerm_virtual_network.vnet-basic.name, subnet_id = azurerm_subnet.subnet-private.id }
  ]
  depends_on = [azurerm_resource_group.rg-basic]
}


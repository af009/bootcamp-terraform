variable "resource-group-name" {
  type = string
}
variable "resource-group-location" {
  type = string
}
variable "admin_username" {
  type = string
}
variable "admin_password" {
  type = string
}
variable "nic-id" {
  type = any
  description = "Network Interface ID"
}
variable "host-ip" {
  type = any
  #azurerm_linux_virtual_machine.vm1.public_ip_address
}
